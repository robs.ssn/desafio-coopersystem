package br.com.robs.rest;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.com.robs.service.IntegracaoBacenService;

@Path("/v1")
public class IntegracaoBacenRest {

	@Inject
	IntegracaoBacenService integracaoBacenService;

	@GET
	@Path("/integracao-bacen")
	@Produces(MediaType.APPLICATION_JSON)
	public Response buscarCotacaoPorData(@QueryParam("dataCotacao") String dataCotacao) {
		return integracaoBacenService.consultarCotacaoPorData(dataCotacao);
	}
}
