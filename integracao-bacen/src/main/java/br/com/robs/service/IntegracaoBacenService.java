package br.com.robs.service;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

import javax.enterprise.context.RequestScoped;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.ServerErrorException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.http.HttpStatus;

import com.google.gson.Gson;

import br.com.robs.dto.ConsultaDolarDto;
import br.com.robs.dto.CotacaoDolarApiDto;
import br.com.robs.dto.RespostaApiDolarDto;
import lombok.extern.slf4j.Slf4j;

@RequestScoped
@Slf4j
public class IntegracaoBacenService {
	
	private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM-dd-yyyy");
	private Client client;
	private final String urlApiCotacaoDolar = "https://olinda.bcb.gov.br/olinda/servico/PTAX/versao/v1/odata/CotacaoDolarDia(dataCotacao=@dataCotacao)";

	public Response consultarCotacaoPorData(String dataCotacao) throws BadRequestException {
		final LocalDate dataInformada = LocalDate.parse(dataCotacao, formatter);
		validacoesDataInformada(dataInformada);
		
		try {
			ConsultaDolarDto cotacao = cotacaoDataInformada(dataInformada);
			return Response.status(HttpStatus.SC_OK).entity(cotacao).build();
		} catch (Exception e) {
			return Response.status(HttpStatus.SC_INTERNAL_SERVER_ERROR).entity(e.getLocalizedMessage()).build();
		}
	}

	private ConsultaDolarDto cotacaoDataInformada(LocalDate dataCotacao) throws Exception {
		final String dataInformada = formatter.format(dataCotacao);
		final LocalDate dataDiaAnterior = dataCotacao.minusDays(1);
		String dataDiaUtilAnterior = diaUtilAnterior(dataDiaAnterior);
		final Gson gson = new Gson();

		try {
			Response respostaDataInformada = getClient(urlApiCotacaoDolar)
					.queryParam("@dataCotacao", "'" + dataInformada + "'")
					.request(MediaType.APPLICATION_JSON)
					.get();
			
			String body1 =  respostaDataInformada.readEntity(String.class);
			RespostaApiDolarDto resposta1 = gson.fromJson(body1, RespostaApiDolarDto.class);

			Response responstaDataDiaUtilAnterior = getClient(urlApiCotacaoDolar)
					.queryParam("@dataCotacao", "'" + dataDiaUtilAnterior + "'" ) 
					.request(MediaType.APPLICATION_JSON)
					.get();
			
			String body2 =  responstaDataDiaUtilAnterior.readEntity(String.class);
			RespostaApiDolarDto resposta2 = gson.fromJson(body2, RespostaApiDolarDto.class);
			
			//Retorno cotacao data informada
			CotacaoDolarApiDto cotacaoDataInformada = resposta1.getValue().stream().findFirst().get();
			Optional<CotacaoDolarApiDto> cotacaoDiaAnteriorOpt = resposta2.getValue().stream().findFirst();
			
			//Retorno cotacao dia ultil anterior a data informada
			CotacaoDolarApiDto cotacaoDiaAnteriorDataInformada  = tratarRetornoConsultaDiaUtil(cotacaoDiaAnteriorOpt);

			return ConsultaDolarDto.builder()
					.vlrCotacaoDataInformada(cotacaoDataInformada.getCotacaoCompra())
					.dtHrCotacaoDataInformada(cotacaoDataInformada.getDataHoraCotacao())
					.vlrCotacaoDiaUtilAnterior(cotacaoDiaAnteriorDataInformada.getCotacaoCompra())
					.dtHrDiaUtilAnteriorDataInformada(cotacaoDiaAnteriorDataInformada.getDataHoraCotacao())
					.build();
		} catch (Exception e) {
			log.error("Excecao ao consultar cotacao dolar ", e);
			throw new ServerErrorException(Response.status(500).entity("Excecao ao consultar cotacao dolar na data ").build());
		} finally {
			closeClient();
		}
	}
	
	private String diaUtilAnterior(LocalDate dataDiaAnterior) {
		if(DayOfWeek.SUNDAY.equals(dataDiaAnterior.getDayOfWeek())) 
			return formatter.format(dataDiaAnterior.minusDays(2));

		if(DayOfWeek.SATURDAY.equals(dataDiaAnterior.getDayOfWeek())) 		
			return formatter.format(dataDiaAnterior.minusDays(1));
		
		return formatter.format(dataDiaAnterior);
	}
	
	private void validacoesDataInformada(LocalDate dataInformada) throws BadRequestException {
		if(dataInformada.isAfter(LocalDate.now()))
			throw new BadRequestException(Response.status(400).entity("A data informada é maior que a data de hoje").build());
		if(dataInformada.getDayOfWeek().equals(DayOfWeek.SATURDAY) || dataInformada.getDayOfWeek().equals(DayOfWeek.SUNDAY))
			throw new BadRequestException(Response.status(400).entity("A data informada não é um dia útil ").build());
	}
	
	private CotacaoDolarApiDto tratarRetornoConsultaDiaUtil(Optional<CotacaoDolarApiDto> cotacaoDiaAnteriorOpt) {
		if(cotacaoDiaAnteriorOpt.isPresent())
			return cotacaoDiaAnteriorOpt.get();
		else
			return CotacaoDolarApiDto.builder()
					.cotacaoCompra(null)
					.dataHoraCotacao("Data dia anterior nao é util")
					.build();
	}
	
	private WebTarget getClient(String url) {
		WebTarget webTarget;
		if (client == null) {
			this.client = ClientBuilder.newClient();
		}
		webTarget = client.target(url);
		return webTarget;
	}

	private void closeClient() {
		try {
			client.close();
			client = null;
		} catch (Exception e) {
			log.error("Nao foi possivel fechar conexao client", e);
		}
	}
}
