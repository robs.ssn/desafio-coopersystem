package br.com.robs.rest;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.http.HttpStatus;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.parameters.Parameter;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponses;

import br.com.robs.service.ConsultaDolarService;

@Path("/v1/cotacoes")
public class ConsultaDolarRest {
	
	@Inject
	ConsultaDolarService consultaDolarService;
	
	@GET
	@Path("/dolar")
	@Produces(MediaType.APPLICATION_JSON)
	@Operation(summary = "Consulta cotacao dolar por data",
				description = "Consulta cotacao dolar de uma data informada e tambem retorno a cotacao do dia util anterior")
	@APIResponses(value = {
			@APIResponse(description = "Consulta realizada com sucesso",responseCode = "200"),
			@APIResponse(description = "Formato de data invalido, data maior que a data atual ou data informada nao e dia util", responseCode = "400"),
			@APIResponse(description = "Erro interno do servidor", responseCode = "500")
		})
	public Response buscarCotacaoPorData(@Parameter(description = "formato MM-DD-YYYY", required = true)
										 @QueryParam("dataCotacao") String dataCotacao) throws WebApplicationException {
			return Response
					.status(HttpStatus.SC_OK)
					.entity(consultaDolarService.consultarCotacaoPorDataViaIntegracao(dataCotacao))
					.build();
	}
}
