package br.com.robs.service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.regex.Pattern;

import javax.enterprise.context.RequestScoped;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.http.HttpStatus;

import com.google.gson.Gson;

import br.com.robs.dto.ConsultaDolarDto;
import br.com.robs.dto.RespostaErroDto;
import lombok.extern.slf4j.Slf4j;

@RequestScoped
@Slf4j
public class ConsultaDolarService {

	private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM-dd-yyyy");
	private Client client;
	private final String urlIntegracaoBacen = "http://localhost:8082/v1/integracao-bacen";
	private final Pattern formatoDataPadraoBacen = Pattern.compile("^\\d{2}-\\d{2}-\\d{4}$");
	
	public ConsultaDolarDto consultarCotacaoPorDataViaIntegracao(String dataCotacao) throws WebApplicationException {
		validarPadraoDataBacen(dataCotacao);
		
		final LocalDate dataInformada = LocalDate.parse(dataCotacao, formatter);
		
		return cotacaoDataInformada(dataInformada);
	}

	private ConsultaDolarDto cotacaoDataInformada(LocalDate dataCotacao) throws WebApplicationException {
		final String dataInformada = formatter.format(dataCotacao);
		final Gson gson = new Gson();

		try {
			Response respostaIntegracaoBacen = getClient(urlIntegracaoBacen)
					.queryParam("dataCotacao", dataInformada)
					.request(MediaType.APPLICATION_JSON).get();

			Integer httpStatus = respostaIntegracaoBacen.getStatus();
			String body = respostaIntegracaoBacen.readEntity(String.class);

			if (respostaIntegracaoBacen.getStatus() != HttpStatus.SC_OK)
				throw new WebApplicationException(Response
								.status(httpStatus)
								.entity(RespostaErroDto.builder()
										.status(httpStatus)
										.mensagem(body).build())
								.build());

			ConsultaDolarDto respostaCotacaoDto = gson.fromJson(body, ConsultaDolarDto.class);

			return ConsultaDolarDto.builder().vlrCotacaoDataInformada(respostaCotacaoDto.getVlrCotacaoDataInformada())
					.dtHrCotacaoDataInformada(respostaCotacaoDto.getDtHrCotacaoDataInformada())
					.vlrCotacaoDiaUtilAnterior(respostaCotacaoDto.getVlrCotacaoDiaUtilAnterior())
					.dtHrDiaUtilAnteriorDataInformada(respostaCotacaoDto.getDtHrDiaUtilAnteriorDataInformada()).build();

		} catch (Exception e) {
			throw e;
		} finally {
			closeClient();
		}
	}
	
	private void validarPadraoDataBacen(String dataCotacao) {
		if (!formatoDataPadraoBacen.matcher(dataCotacao).matches())
			throw new WebApplicationException(Response.status(400).entity(RespostaErroDto.builder().status(400)
							.mensagem("Data informada " + dataCotacao +" inválida, deve ser no padrão MM-DD-YYYY").build())
						.build());
	}
	
	private WebTarget getClient(String url) {
		WebTarget webTarget;
		if (client == null) {
			this.client = ClientBuilder.newClient();
		}

		webTarget = client.target(url);
		return webTarget;
	}

	private void closeClient() {
		try {
			client.close();
			client = null;
		} catch (Exception e) {
			log.error("Nao foi possivel fecha conexao client", e);
		}
	}
}
