package br.com.robs.dto;

import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Builder.Default;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RespostaErroDto {

	private Integer status;
	private String mensagem;
	@Default
	private LocalDateTime dataHora = LocalDateTime.now();
}
