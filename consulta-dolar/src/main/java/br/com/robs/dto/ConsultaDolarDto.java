package br.com.robs.dto;

import java.math.BigDecimal;

import org.eclipse.microprofile.openapi.annotations.media.Schema;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Schema(name = "Retorno consulta")
public class ConsultaDolarDto {
	
	@JsonProperty(index = 0)
	@Schema(description = "Valor da cotacao da data informada")
	private BigDecimal vlrCotacaoDataInformada;
	
	@JsonProperty(index = 1)
	@Schema(description = "Data da cotacao da data informada")
	private String dtHrCotacaoDataInformada;

	@JsonProperty(index = 2)
	@Schema(description = "Valor da cotacao do dia util anterior a data informada")
	private BigDecimal vlrCotacaoDiaUtilAnterior;
	
	@JsonProperty(index = 3)
	@Schema(description = "Data da cotacao do dia util anterior a data informada")
	private String dtHrDiaUtilAnteriorDataInformada;
	
}
